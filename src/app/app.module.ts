import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {
  NbMenuModule,
  NbSidebarModule,
} from '@nebular/theme';
import { StateModule } from './pages/states/state.module';
import { InvoiceGuard } from './guards/invoice.guard';
import { RoomGuard } from './guards/room.guard';
import { TenantDetailsGuard } from './guards/tenat-details.guard';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    CoreModule.forRoot(),
    ThemeModule.forRoot(),
    StateModule
  ],
  providers:[InvoiceGuard,RoomGuard,TenantDetailsGuard],
  bootstrap: [AppComponent],
})
export class AppModule {
}
