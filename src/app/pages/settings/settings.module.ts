import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SettingsComponent } from './settings.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NbCardModule } from '@nebular/theme';
import { SettingsService } from '../../services/settings.service';


const routes:Routes=[
    {
        path:'',
        component:SettingsComponent
    }
]

@NgModule({
    declarations: [
        SettingsComponent
    ],
    imports: [ 
        CommonModule,
        NbCardModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [
        SettingsService
    ],
})
export class SettingsModule {}