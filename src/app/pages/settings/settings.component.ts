import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SettingsService } from '../../services/settings.service';
import { SettingsState } from '../states/settings.state';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

    private subscription$=new Subject();
    formGroup:FormGroup;
    loading:boolean=false;
    constructor(private service:SettingsService,public state:SettingsState,private fb:FormBuilder) {
        this.formGroup=this.fb.group(
            {
                sms:new FormGroup(
                    {
                        message:new FormControl('',Validators.required)
                    }
                ),
                settings:new FormGroup(
                    {
                        dollarRate:new FormControl('',Validators.required)
                    }
                ),
                utility:new FormGroup(
                    {
                        "type":new FormControl('',Validators.required),
                        "amount":new FormControl('',Validators.required)
                    }
                ),
                contacts:new FormGroup(
                    {
                        "contact":new FormControl('',Validators.required)
                    }
                )
            }
        )
    };


    ngOnInit(): void {
        this.service.loadSms();
        this.service.loadUtilities();
        this.service.loadSettings();
    };


    deleteAccount(cont:string):void{
        const confirmed=window.confirm('Do you want to delete this contact');
        if(!confirmed)return;
        let {receive_sms_alerts}=this.state.settings$.value;
        let filter=receive_sms_alerts.filter(c=>c!==cont);
        let data={receive_sms_alerts:filter};
        this.service.addSettings(data).pipe(takeUntil(this.subscription$))
                .subscribe((response:any)=>{
                    this.loading=false;
                    this.service.loadSettings();
                    alert(response.message);
                })

    }

    saveAccount():void{
        const contacts=this.formGroup.get('contacts');
        if(!contacts.valid)return;
        const confirmed=window.confirm('Do you want to add this contact');
        if(!confirmed)return;
        let {receive_sms_alerts}=this.state.settings$.value;
        let data={receive_sms_alerts:[...receive_sms_alerts,contacts.value.contact]};
        this.service.addSettings(data).pipe(takeUntil(this.subscription$))
                .subscribe((response:any)=>{
                    this.loading=false;
                    this.service.loadSettings();
                    contacts.reset();
                    alert(response.message);
                })

    }


    saveSettings():void{
        const settings=this.formGroup.get('settings');
        if(!settings.valid)return;
        const confirmed=window.confirm('Do you want to update your settings');
        if(!confirmed)return;
        this.loading=true;
        this.service.addSettings(settings.value).pipe(takeUntil(this.subscription$))
                .subscribe((response:any)=>{
                    this.loading=false;
                    this.service.loadSettings();
                    settings.reset();
                    alert(response.message);
                })

    }


    deleteUtility(id:string):void{
        const confirmed=window.confirm('Do you want to delete this utility');
        if(!confirmed)return;
        this.service.deleteUtility(id).pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                this.loading=false;
                this.service.loadUtilities();
                alert(response.message);
            })

    }

    saveUtility():void{
        const utility=this.formGroup.get('utility');
        if(!utility.valid)return;
        const confirmed=window.confirm('Do you want to add this utility');
        if(!confirmed)return;
        this.loading=true;
        this.service.addUtility(utility.value).pipe(takeUntil(this.subscription$))
                .subscribe((response:any)=>{
                    this.loading=false;
                    this.service.loadUtilities();
                    utility.reset();
                    alert(response.message);
                })

    }

    send():void{
        const sms=this.formGroup.get('sms');
        if(!sms.valid)return;
        const confirmed=window.confirm('Do you want to send this message to all tenants');
        if(!confirmed)return;
        this.loading=true;
        this.service.sendSms(sms.value).pipe(takeUntil(this.subscription$))
                .subscribe((response:any)=>{
                    this.loading=false;
                    this.service.loadSms();
                    sms.reset();
                    alert(response.message);
                })

    }


    ngOnDestroy(): void {
        this.subscription$.next();
        this.subscription$.complete();
    }

    
}
