import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable,combineLatest, Subscription, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { StatusType, TenantStatus } from '../../../models/common.model';
import { TenantInterface } from '../../../models/tenant.model';
import { TenantService } from '../../../services/tenants.service';
import { FilterState } from '../../states/filter.state';
import { TenantState } from '../../states/tenant.state';

@Component({
    selector: 'app-alltenants',
    templateUrl: './alltenants.component.html',
    styleUrls: ['./alltenants.component.scss']
})
export class AllTenantsComponent implements OnInit,OnDestroy {

    tenantStatus:TenantStatus;
    filter:Array<TenantStatus>=[TenantStatus.ALL,TenantStatus.true,TenantStatus.false];
    tenants$:Observable<TenantInterface[]>;
    private subsription$=new Subject();

    constructor(private service:TenantService,private state:TenantState,private filter$:FilterState,private router:Router) {
        this.tenants$=combineLatest([this.state.tenants$,this.filter$.filter$,this.filter$.searchTerm$]).pipe(
            map(([tenants,filter,search]:[TenantInterface[],TenantStatus,string])=>{
                let _filter=filter===TenantStatus.ALL?tenants:tenants.filter(r=>r.approved===StatusType[filter]);
                return _filter.filter(r=>{
                    let name=`${r.firstname} ${r.lastname}`.toLowerCase();
                    return name.indexOf(search.toLowerCase())>-1;
                })
            })
        )
    };


    ngOnInit(): void {
        this.service.fetchTenants();
    }

    view(d:TenantInterface,path:string){
        this.state.setSelected(d);
        console.log('jell');
        
        this.router.navigate([`home/tenants/${path}`]);
    }

    onFilter(e:Event|any){
        e.preventDefault();
        const d:StatusType=e.target.value;
        this.filter$.setFilter(
            d
        )
    }

    approve(tenant:TenantInterface):void{
        const data={id:tenant._id,data:{ approved:!tenant.approved }}
        this.service.update(data).pipe(takeUntil(this.subsription$))
            .subscribe((response:any)=>{
                alert(response.message);
                this.service.fetchTenants();
            })
    }

    block(tenant:TenantInterface):void{
        const data={id:tenant._id,data:{ blocked:!tenant.blocked }}
        this.service.update(data).pipe(takeUntil(this.subsription$))
            .subscribe((response:any)=>{
                alert(response.message);
                this.service.fetchTenants();
            })
    }


    ngOnDestroy(): void {
        this.subsription$.next();
        this.subsription$.complete();
    }



}
