import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllTenantsComponent } from './alltenants/alltenants.component';
import { RouterModule, Routes } from '@angular/router';
import { NbCardModule, NbInputModule, NbSelectModule } from '@nebular/theme';
import { TenantsComponent } from './tenants.component';
import { TenantDetailsComponent } from './tenant-details/tenant-details.component';
import { TenantDetailsGuard } from '../../guards/tenat-details.guard';

const routes:Routes=[
    {
        path:'',
        component:TenantsComponent,
        children:[
            {
                path:'',
                component:AllTenantsComponent
            },
            {
                path:'details',
                canActivate:[TenantDetailsGuard],
                component:TenantDetailsComponent
            }
        ]
    }
]

@NgModule({
    declarations: [
        AllTenantsComponent,
        TenantsComponent,
        TenantDetailsComponent
    ],
    imports: [ 
        CommonModule,
        NbCardModule,
        NbInputModule,
        NbSelectModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [],
})
export class TenantsModule {}