import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TenantInterface } from '../../../models/tenant.model';
import { TenantState } from '../../states/tenant.state';

@Component({
    selector: 'app-tenant-details',
    templateUrl: './tenant-details.component.html',
    styleUrls: ['./tenant-details.component.scss']
})
export class TenantDetailsComponent implements OnInit {

    details:any=[];
    selected:TenantInterface;
    private subsciption$=new Subject();

    constructor(private state:TenantState) {
        this.state.selected$.pipe(takeUntil(this.subsciption$))
                .subscribe(d=>{
                    this.selected=d;
                    this.details=Object.keys(d).filter(d=>d!=='photo' && d!=='_id' && d!=='password').map(k=>{return [k,d[k]] });
                })
    };

    ngOnInit(): void {};


    ngOnDestroy(): void {
        this.subsciption$.next();
        this.subsciption$.complete();
    }



}
