import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-tenants',
    template: '<router-outlet></router-outlet>'
})
export class TenantsComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {}


}
