import { ThisReceiver } from '@angular/compiler';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { iif, Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { ApartmentInterface } from '../../../models/apartment.model';
import { PathTypes } from '../../../models/common.model';
import { RoomService } from '../../../services/rooms.service';
import { RoomState } from '../../states/rooms.state';

@Component({
    selector: 'app-add-apartment',
    templateUrl: './add-apartment.component.html',
    styleUrls: ['./add-apartment.component.scss']
})
export class AddApartmentComponent implements OnInit,OnDestroy {

    pathTypes=PathTypes;
    path:string='';
    private subscription$=new Subject();
    private selectedId:string;
    loading:boolean=false;

    formGroup:FormGroup;
    constructor(private state:RoomState,private route:ActivatedRoute,private service:RoomService) {
        this.formGroup=new FormGroup(
            {
                "type":new FormControl({value:'',disabled:false},Validators.required),
                "location":new FormControl({value:'',disabled:false},Validators.required),
                "address":new FormControl({value:'',disabled:false},Validators.required),
                "size":new FormControl({value:'',disabled:false},Validators.required),
                "condition":new FormControl({value:'',disabled:false},Validators.required),
                "furnishing":new FormControl({value:'',disabled:false},Validators.required),
                "roomNumber":new FormControl({value:'',disabled:false},Validators.required),
                "price":new FormControl({value:'',disabled:false},Validators.required),
                "details":new FormControl({value:'',disabled:false},Validators.required),
                "accomodation":new FormControl({value:'',disabled:false},Validators.required),
                "inspectionAmount":new FormControl({value:'',disabled:false},Validators.required),
                "currency":new FormControl({value:'',disabled:false},Validators.required),
                "images":new FormControl([])
            }
        )
    };


    ngAfterContentInit(): void {
        this.state.selected$.pipe(takeUntil(this.subscription$))
            .subscribe((d:ApartmentInterface)=>{
                if(this.path !== this.pathTypes.ADD && d && d._id){
                    this.selectedId=d._id;
                    this.formGroup.patchValue(d);
                }
            })
    }



    ngOnInit(): void {
        this.setView();
    };



    setView(){
        this.path=this.route.snapshot.url[0].path;
        switch (this.path) {
            case this.pathTypes.DETAILS:
                this.formGroup.disable();
                break;
            case this.pathTypes.ADD:
                this.formGroup.enable();
                break;
            default:
                this.formGroup.enable();
                break;
        }
    }

    save(){
        switch (this.path) {
            case this.pathTypes.ADD:
                this.addRoom();
                break;
            case this.pathTypes.EDIT:
                this.updateRoom();
                break;
            default:
                break;
        }
    }


    removeImage(d:any):void{
        const images:string[]=this.formGroup.get('images').value;
        let filter:string[]=[];
        switch (this.path) {
            case this.pathTypes.EDIT:
                filter=images.filter(img=>img!==d);
                break;
            case this.pathTypes.ADD:
                filter=images.filter((img:any)=>img.data!==d['data']);
                break;
            default:
                break;
        }
        this.formGroup.patchValue(
            {
                images:filter
            }
        )

    }

    updateRoom():void{
        const selected=this.state.selected$.value;
        if(!selected._id)return;
        this.loading=true;
        this.service.upateRoom({id:selected._id,data:this.formGroup.value})
                .pipe(takeUntil(this.subscription$))
                .subscribe((response:any)=>{
                    this.loading=false;
                    alert(response.message);
                })
    }


    uploadImage(image:{data:string|ArrayBuffer,type:string}){
        const selected=this.state.selected$.value;
        if(!selected._id)return;
        this.loading=true;
        this.service.uploadImage({id:selected._id,images:[image]})
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                this.loading=false;
                alert(response.message);
            })
    }


    addRoom():void{
        if(!this.formGroup.valid)return;
        if(this.formGroup.get('images').value.length<=0){
            return alert(
                'Please add images to this apartment'
            )
        }
        this.loading=true;
        this.service.addRoom(this.formGroup.value)
            .pipe(takeUntil(this.subscription$))
            .subscribe(response=>{
                this.loading=false;
                this.formGroup.reset();
                alert(response.message)
            })
    }


    onPicker(d:File){
        const fileReader=new FileReader();
        fileReader.onloadend=()=>{
            const res=fileReader.result;
            let images=this.formGroup.get('images').value || [];
            const image={data:res,type:d.type.split('/')[1]};
            this.formGroup.patchValue(
                {
                    images:[...images,image]
                }
            )
            if(this.path!==PathTypes.ADD){
                this.uploadImage(image);
            }
        }
        fileReader.readAsDataURL(d);
    }


    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.subscription$.next();
        this.subscription$.complete();
        this.formGroup.reset();
    }


}
