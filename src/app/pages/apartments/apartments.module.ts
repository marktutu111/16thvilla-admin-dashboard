import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AvailabeApartmentsComponent } from './available-apartments/available-apartments.component';
import { ApartmentsComponent } from './apartments.component';
import {NbCardModule, NbDialogModule} from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { AddApartmentComponent } from './add-apartment/add-apartment.component';
import { RoomService } from '../../services/rooms.service';
import { ReactiveFormsModule } from '@angular/forms';
import { RoomGuard } from '../../guards/room.guard';


const routes:Routes=[
    {
        path:'',
        component:ApartmentsComponent,
        children:[
            {
                path:'',
                redirectTo:'available'
            },
            {
                path:'available',
                component:AvailabeApartmentsComponent
            },
            {
                path:'details',
                canActivate:[RoomGuard],
                component:AddApartmentComponent
            },
            {
                path:'edit',
                canActivate:[RoomGuard],
                component:AddApartmentComponent
            },
            {
                path:'add',
                component:AddApartmentComponent
            }
        ]
    }
]

@NgModule({
    declarations: [
        ApartmentsComponent,
        AvailabeApartmentsComponent,
        AddApartmentComponent
    ],
    imports: [ 
        CommonModule,
        ReactiveFormsModule,
        NbCardModule,
        NbDialogModule.forChild(),
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [
        RoomService
    ],
})
export class ApartmentsModule {}