import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ApartmentInterface } from '../../../models/apartment.model';
import { PathTypes } from '../../../models/common.model';
import { RoomService } from '../../../services/rooms.service';
import { SettingsService } from '../../../services/settings.service';
import { RoomState } from '../../states/rooms.state';
import { SettingsState } from '../../states/settings.state';


@Component({
  selector: 'app-available-apartments',
  templateUrl: './available-apartments.component.html',
  styleUrls: ['./available-apartments.component.scss'],
})
export class AvailabeApartmentsComponent implements OnInit,OnDestroy {
  
  paths=PathTypes;
  private subscription$=new Subject();
  loading:boolean=false;


  constructor(
      public roomState:RoomState,
      public settings:SettingsState,
      private service:RoomService,
      private router:Router,
      private ss:SettingsService
    ) {};


    view(d:ApartmentInterface,path:string){
      this.router.navigate([`home/apartments/${path}`]);
      this.roomState.setSelected(
        d
      )
    }


  ngOnInit(): void {
    this.ss.loadSettings();
    this.service.fetchRooms();

  }

  deleteRoom(id:string):void{
    const prompt=window.confirm('Confirm Delete');
    if(!prompt)return;
    this.loading=true;
    this.service.deleteRoom(id).pipe(takeUntil(this.subscription$))
        .subscribe((response:any)=>{
            this.loading=false;
            alert(response.message);
            this.service.fetchRooms();
        })
  }


  ngOnDestroy(): void {
      this.subscription$.next();
      this.subscription$.complete();
  }


}
