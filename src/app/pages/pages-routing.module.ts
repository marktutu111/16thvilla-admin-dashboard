import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path:'apartments',
      loadChildren:()=>import('./apartments/apartments.module')
          .then(m=>m.ApartmentsModule)
    },
    {
      path:'rents',
      loadChildren:()=>import('./rents/rents.module')
          .then(m=>m.RentsModule)
    },
    {
      path:'tenants',
      loadChildren:()=>import('./tenants/tenant.module')
          .then(m=>m.TenantsModule)
    },
    {
      path:'payments',
      loadChildren:()=>import('./payments/payments.module')
          .then(m=>m.PaymentsModule)
    },
    {
      path:'inspections',
      loadChildren:()=>import('./inspections/inspections.module')
          .then(m=>m.InspectionsModule)
    },
    {
      path:'dashboard',
      loadChildren:()=>import('./dashboard/dashboard.module')
          .then(m=>m.DashboardModule)
    },
    {
      path:'expenses',
      loadChildren:()=>import('./expenses/expenses.module')
          .then(m=>m.ExpensesModule)
    },
    {
      path:'invoice',
      loadChildren:()=>import('./invoice/invoice.module')
          .then(m=>m.InvoiceModule)
    },
    {
      path:'settings',
      loadChildren:()=>import('./settings/settings.module')
          .then(m=>m.SettingsModule)
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: '**',
      redirectTo:''
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
