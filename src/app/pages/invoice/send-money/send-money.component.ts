import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { InvoiceService } from '../../../services/invoice.service';
import { InvoiceState } from '../../states/invoice.state';

@Component({
    selector: 'app-send-money',
    templateUrl: './send-money.component.html',
    styleUrls: ['./send-money.component.scss']
})
export class SendMoneyComponent implements OnInit {

    private subscription$=new Subject();
    formGroup:FormGroup;
    loading:boolean=false;

    constructor(private state:InvoiceState,private service:InvoiceService) {
        this.formGroup=new FormGroup(
            {
                id:new FormControl('',Validators.required),
                accountNumber:new FormControl('',Validators.required),
                accountIssuer:new FormControl('',Validators.required),
                amount:new FormControl('',Validators.required)
            }
        )
    }

    ngOnInit(): void {
        this.state.selected$.pipe(takeUntil(this.subscription$))
            .subscribe((invoice:any)=>{
                if(invoice && invoice._id){
                    this.formGroup.patchValue(
                        {
                            id:invoice._id,
                            amount:invoice.total
                        }
                    )
                }
            })
    };


    send():void{
        if(!this.formGroup.valid)return;
        const {amount,accountNumber}=this.formGroup.value;
        const confirm=window.confirm(`You have requested to send an amount of GHS${amount} to ${accountNumber}. Do you confirm this transaction?`);
        if(!confirm)return;
        this.loading=true;
        this.service.sendMoney(this.formGroup.value)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                alert(response.message);
                this.loading=false;
            })
    }


    ngOnDestroy(): void {
        this.subscription$.next();
        this.subscription$.complete();
    }



}
