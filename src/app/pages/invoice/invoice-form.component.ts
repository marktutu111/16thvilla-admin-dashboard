import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { InvoicePayload } from '../../models/invoice.model';
import { InvoiceService } from '../../services/invoice.service';
import { InvoiceState } from '../states/invoice.state';

@Component({
    selector: 'app-invoice-form',
    templateUrl: './invoice-form.component.html',
    styleUrls: ['./invoice-form.component.scss']
})
export class InvoiceFormComponent implements OnInit,OnDestroy {

    loading:boolean=false;
    formGroup:FormGroup;

    private subscription$=new Subject();


    constructor(private state:InvoiceState,private invoice:InvoiceService) {
        this.formGroup=new FormGroup(
            {
                customerName:new FormControl('',Validators.required),
                customerPhone:new FormControl('',Validators.required),
                customerAddress:new FormControl('',Validators.required),
                total:new FormControl(0,Validators.required),
                vat:new FormControl(0,Validators.required),
                subtotal:new FormControl('',Validators.required),
                status:new FormControl('pending'),
                amount:new FormControl(0,Validators.required),
                item:new FormControl('',Validators.required),
                description:new FormControl('',Validators.required)
            }
        )
    };

    ngOnInit(): void {
        this.formGroup.valueChanges.pipe(takeUntil(this.subscription$)).subscribe(({vat,amount})=>{
            try {
                if(typeof vat==='number'){
                    const _amount=(vat/100)*amount;
                    this.formGroup.patchValue(
                        {
                            total:amount-_amount,
                            subtotal:amount
                        },
                        {
                            emitEvent:false,
                            onlySelf:true
                        }
                    )
                }
            } catch (err) {}
        })
    };


    save(){
        if(!this.formGroup.valid){
            return;
        }
        this.loading=true;
        const _payload:InvoicePayload|any=this.formGroup.value;
        delete _payload.amount;
        this.invoice.addInvoice(_payload).pipe(takeUntil(this.subscription$))
            .subscribe(response=>{
                alert(response.message)
                this.formGroup.reset();
                this.loading=false;
                this.invoice.getInvoices();
            })
    }



    ngOnDestroy(){
        this.subscription$.next();
        this.subscription$.complete();
    }



}
