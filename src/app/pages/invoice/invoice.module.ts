import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceComponent } from './invoice.component';
import { InvoiceListComponent } from './invoice-list.component';
import { RouterModule, Routes } from '@angular/router';
import { InvoiceService } from '../../services/invoice.service';
import { InvoiceFormComponent } from './invoice-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NbCardModule } from '@nebular/theme';
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module
import { SendMoneyComponent } from './send-money/send-money.component';
import { InvoiceGuard } from '../../guards/invoice.guard';



const routes:Routes=[
    {
        path:'',
        component:InvoiceListComponent,
    },
    {
        path:'details',
        canActivate:[InvoiceGuard],
        component:InvoiceComponent
    },
    {
        path:'new',
        component:InvoiceFormComponent
    },
    {
        path:'sendmoney',
        canActivate:[InvoiceGuard],
        component:SendMoneyComponent
    }
]

@NgModule({
    declarations: [
        InvoiceComponent,
        InvoiceListComponent,
        InvoiceFormComponent,
        SendMoneyComponent
    ],
    imports: [ 
        CommonModule,
        ReactiveFormsModule,
        NbCardModule,
        NgxPaginationModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [
        InvoiceService
    ],
})
export class InvoiceModule {}