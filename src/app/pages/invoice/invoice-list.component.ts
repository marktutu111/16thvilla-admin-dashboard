import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { InvoiceModel } from '../../models/invoice.model';
import { InvoiceService } from '../../services/invoice.service';
import { InvoiceState } from '../states/invoice.state';

@Component({
    selector: 'app-invoice-list',
    templateUrl: './invoice-list.component.html',
    styleUrls: ['./invoice-list.component.scss']
})
export class InvoiceListComponent implements OnInit {

    loading$:Observable<boolean>;
    p:number=1;

    constructor(public state:InvoiceState,private router:Router,private service:InvoiceService) {};


    ngOnInit(): void {
        this.service.getInvoices();
    };


    view(inv:InvoiceModel){
        this.router.navigate(['home/invoice/details'])
        this.state.setSelected(
            inv
        )
    }


}
