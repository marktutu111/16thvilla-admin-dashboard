import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { InvoiceModel } from '../../models/invoice.model';
import { InvoiceService } from '../../services/invoice.service';
import { InvoiceState } from '../states/invoice.state';

@Component({
    selector: 'app-invoice',
    templateUrl: './invoice.component.html',
    styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {

    private subscription$=new Subject();
    loading:boolean=false;

    constructor(private service:InvoiceService,public state:InvoiceState,private router:Router) {}

    ngOnInit(): void {};


    async pay(id:string){
        if(!id){
            return;
        };
        this.loading=true;
        this.service.payInvoice(id).pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                this.loading=false;
                alert(
                    response.message
                )
            })
    }

    sendMoney(){
        this.router.navigate(['../home/invoice/sendmoney'])
    }

    print(){
        window.print();
    }


}
