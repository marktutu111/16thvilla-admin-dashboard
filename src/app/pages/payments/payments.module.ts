import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbCardModule } from '@nebular/theme';
import { RouterModule, Routes } from '@angular/router';
import { PaymentsComponent } from './payments.component';
import { PaymentService } from '../../services/payments.service';
import { NgxPaginationModule } from 'ngx-pagination';

const routes:Routes=[
    {
        path:'',
        component:PaymentsComponent
    }
]

@NgModule({
    declarations: [
        PaymentsComponent
    ],
    imports: [ 
        CommonModule,
        NbCardModule,
        NgxPaginationModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [
        PaymentService
    ],
})
export class PaymentsModule {};