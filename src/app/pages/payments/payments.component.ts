import { Component, OnDestroy, OnInit } from '@angular/core';
import { resourceUsage } from 'process';
import { Observable, Subscription,combineLatest, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { NetworkType, TransactionStatus } from '../../models/common.model';
import { PaymentInterface } from '../../models/payment.model';
import { PaymentService } from '../../services/payments.service';
import { PaymentState } from '../states/payment.state';

@Component({
    selector: 'app-payments',
    templateUrl: './payments.component.html',
    styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit,OnDestroy {

    tranStatus=TransactionStatus;
    networkType=NetworkType;
    filter:TransactionStatus[]=[
        TransactionStatus.ALL,
        TransactionStatus.FAILED,
        TransactionStatus.PAID,
        TransactionStatus.PENDING,
        TransactionStatus.REVERSED
    ]

    p:number=1;
    payments$:Observable<PaymentInterface[]>;
    private subscription$=new Subject();

    constructor(private service:PaymentService, private state:PaymentState) {
        this.payments$=combineLatest([this.state.payments$]).pipe(
            map(([payments]:[PaymentInterface[]])=>{
                return payments;
            })
        )
    };

    ngOnInit(): void {
        this.loadPayments();
    };


    delete(id:string):void{
        const confirm=window.confirm('Do you want to delete this transaction');
        if(!confirm)return;
        this.service.deleteTransaction(id)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                alert(response.message);
                this.loadPayments();
            })
    }


    loadPayments():void {
        this.service.fetchPayments().pipe(takeUntil(this.subscription$)).subscribe(
            response=>{
                if(response.success){
                    this.state.addPayment(
                        response.data
                    )
                }
            }
        )
    }


    ngOnDestroy(): void {
        this.subscription$.next();
        this.subscription$.complete();
    }




}
