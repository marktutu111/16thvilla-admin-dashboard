import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { PaymentInterface } from '../../models/payment.model';

@Injectable({
    providedIn: 'root'
})
export class PaymentState {

    payments$:BehaviorSubject<PaymentInterface[]>=new BehaviorSubject([]);

    constructor(){};

    addPayment(data:Array<PaymentInterface>){
        this.payments$.next(
            data
        )
    }

}