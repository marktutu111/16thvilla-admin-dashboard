import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { InspectionInterface } from '../../models/inspections.model';

@Injectable({
    providedIn: 'root'
})
export class InspectionState {

    inspections$:BehaviorSubject<InspectionInterface[]>=new BehaviorSubject([]);

    constructor(){};

    addInspections(data:Array<InspectionInterface>){
        this.inspections$.next(
            data
        )
    }

}