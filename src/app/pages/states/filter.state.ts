import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { StatusType, TenantStatus } from '../../models/common.model';
import { DashboardInterface } from '../../models/dashboard.model';

@Injectable({
    providedIn: 'root'
})
export class FilterState {

    expenseSummary$:BehaviorSubject<DashboardInterface>=new BehaviorSubject(null);
    invoiceSummary$:BehaviorSubject<DashboardInterface>=new BehaviorSubject(null);
    dashboard$:BehaviorSubject<DashboardInterface>=new BehaviorSubject(null);
    filter$:BehaviorSubject<StatusType|TenantStatus>=new BehaviorSubject(StatusType.ALL);
    searchTerm$:BehaviorSubject<string>=new BehaviorSubject('');

    constructor(){};

    setFilter(filter:StatusType|TenantStatus){
        this.filter$.next(filter);
    }

    onSearch(term:string){
        this.searchTerm$.next(
            term
        )
    }

    updateDashboard(d:DashboardInterface):void{
        this.dashboard$.next(
            d
        )
    }

    updateExpense(d):void{
        this.expenseSummary$.next(
            d
        )
    }

    updateInvoice(d):void{
        this.invoiceSummary$.next(
            d
        )
    }


    
}