import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { ExpenseCategory, ExpenseModel } from '../../models/expenses.model';

@Injectable({
    providedIn: 'root'
})
export class ExpenseState {

    expenses$:BehaviorSubject<ExpenseModel[]>=new BehaviorSubject([]);
    categories$:BehaviorSubject<ExpenseCategory[]>=new BehaviorSubject([]);
    summary$:BehaviorSubject<{total:number,count:number}>=new BehaviorSubject({total:0,count:0})

    constructor(){};

    addExpenses(d:ExpenseModel[]):void{
        this.expenses$.next(
            d
        )
    }

    addCategory(d:ExpenseCategory[]):void{
        this.categories$.next(
            d
        )
    }

    addSummary(d:any):void{
        this.summary$.next(
            d
        )
    }


}