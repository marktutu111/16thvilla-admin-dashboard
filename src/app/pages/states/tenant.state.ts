import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TenantInterface } from '../../models/tenant.model';

@Injectable({
    providedIn: 'root'
})
export class TenantState {

    tenants$:BehaviorSubject<TenantInterface[]>=new BehaviorSubject([]);
    selected$:BehaviorSubject<TenantInterface>=new BehaviorSubject(null);

    constructor(){};

    setSelected(data:TenantInterface):void{
        this.selected$.next(
            data
        )
    }

    addTenents(data:Array<TenantInterface>){
        this.tenants$.next(
            data
        )
    }


}