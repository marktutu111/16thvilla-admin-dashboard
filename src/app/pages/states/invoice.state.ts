import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { InvoiceModel } from '../../models/invoice.model';

@Injectable({
    providedIn: 'root'
})
export class InvoiceState {
    summary$:BehaviorSubject<{total:number,count:number}>=new BehaviorSubject({total:0,count:0});
    invoices$:BehaviorSubject<InvoiceModel[]>=new BehaviorSubject([]);
    selected$:BehaviorSubject<InvoiceModel>=new BehaviorSubject(null);

    constructor(){}

    addInvoices(d:InvoiceModel[]):void{
        this.invoices$.next(
            d
        )
    }

    addSummary(d):void{
        this.summary$.next(
            d
        )
    }

    setSelected(d:InvoiceModel):void{
        this.selected$.next(
            d
        )
    }

}