import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomState } from './rooms.state';
import { RentState } from './rents.state';
import { FilterState } from './filter.state';
import { PaymentState } from './payment.state';
import { InspectionState } from './inspection.state';
import { InspectionService } from '../../services/insepection.service';
import { RentService } from '../../services/rent.service';
import { TenantService } from '../../services/tenants.service';
import { ExpenseState } from './expenses.state';
import { InvoiceState } from './invoice.state';
import { SettingsState } from './settings.state';

@NgModule({
    imports: [ CommonModule ],
    providers: [
        FilterState,
        RoomState,
        RentState,
        PaymentState,
        InspectionState,
        ExpenseState,
        InspectionService,
        RentService,
        TenantService,
        InvoiceState,
        SettingsState
    ],
})
export class StateModule {}