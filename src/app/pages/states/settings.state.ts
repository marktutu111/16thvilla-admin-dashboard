import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SettingsInterface, SmsInterface } from '../../models/settings.model';
import { UtilitiesInterface } from '../../models/utilities.model';

@Injectable({
    providedIn: 'root'
})
export class SettingsState {

    utilities$:BehaviorSubject<UtilitiesInterface[]>=new BehaviorSubject([]);
    settings$:BehaviorSubject<SettingsInterface>=new BehaviorSubject({dollarRate:0,smsBalance:0,receive_sms_alerts:[]});
    messages$:BehaviorSubject<SmsInterface[]>=new BehaviorSubject([]);

    constructor(){};

    addMessages(d:SmsInterface[]):void{
        this.messages$.next(
            d
        )
    }

    addSettings(d):void{
        this.settings$.next(
            d
        )
    }

    addUtilities(d:UtilitiesInterface[]):void{
        this.utilities$.next(
            d
        )
    }


}