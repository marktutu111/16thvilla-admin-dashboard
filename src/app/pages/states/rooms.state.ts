import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApartmentInterface } from '../../models/apartment.model';
import { StatusType } from '../../models/common.model';

@Injectable({
    providedIn: 'root'
})
export class RoomState {

    selected$:BehaviorSubject<ApartmentInterface>=new BehaviorSubject(null);
    apartments$:BehaviorSubject<ApartmentInterface[]>=new BehaviorSubject([]);
    filter$:BehaviorSubject<StatusType>=new BehaviorSubject(StatusType.ALL);

    constructor(){};

    addRooms(data:Array<ApartmentInterface>){
        this.apartments$.next(
            data
        )
    }

    setFilter(filter:StatusType){
        this.filter$.next(filter);
    }

    setSelected(data:ApartmentInterface){
        this.selected$.next(
            data
        )
    }
    


}