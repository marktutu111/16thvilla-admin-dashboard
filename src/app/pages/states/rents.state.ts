import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { StatusType } from '../../models/common.model';
import { RentInterface } from '../../models/rents.model';

@Injectable({
    providedIn: 'root'
})
export class RentState {

    rents$:BehaviorSubject<RentInterface[]>=new BehaviorSubject([]);
    filter$:BehaviorSubject<StatusType>=new BehaviorSubject(StatusType.ALL);
    searchTerm$:BehaviorSubject<string>=new BehaviorSubject('');
    pending$:BehaviorSubject<RentInterface[]>=new BehaviorSubject([]);

    constructor(){};

    addRent(data:Array<RentInterface>){
        this.rents$.next(
            data
        )
    }

    setPending(data:RentInterface[]):void{
        this.pending$.next(
            data
        )
    }

    setFilter(filter:StatusType){
        this.filter$.next(filter);
    }

    onSearch(term:string){
        this.searchTerm$.next(
            term
        )
    }


}