import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbSearchService } from '@nebular/theme';
import { Subscription } from 'rxjs';

import { MENU_ITEMS } from './pages-menu';
import { FilterState } from './states/filter.state';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent implements OnInit,OnDestroy {

  private _search$:Subscription;
  menu = MENU_ITEMS;
  constructor(private search$:NbSearchService,private filter:FilterState){};

  ngOnInit(): void {
    this._search$=this.search$.onSearchSubmit().subscribe((search)=>this.filter.onSearch(search.term || ''));
  }

  ngOnDestroy(): void {
    this._search$.unsubscribe();
  }


}
