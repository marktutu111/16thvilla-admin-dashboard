import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription,combineLatest, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { NetworkType, TransactionStatus } from '../../models/common.model';
import { InspectionInterface } from '../../models/inspections.model';
import { InspectionService } from '../../services/insepection.service';
import { InspectionState } from '../states/inspection.state';

@Component({
    selector: 'app-inspections',
    templateUrl: './inspections.component.html',
    styleUrls: ['./inspections.component.scss']
})
export class InspectionsComponent implements OnInit,OnDestroy {

    tranStatus=TransactionStatus;
    networkType=NetworkType;
    filter:TransactionStatus[]=[
        TransactionStatus.ALL,
        TransactionStatus.FAILED,
        TransactionStatus.PAID,
        TransactionStatus.PENDING,
        TransactionStatus.REVERSED
    ]

    inspections$:Observable<InspectionInterface[]>;
    private subscription$=new Subject();

    constructor(private service:InspectionService,private state:InspectionState) {
        this.inspections$=combineLatest([this.state.inspections$]).pipe(
            takeUntil(this.subscription$),
            map(([inspections]:[InspectionInterface[]])=>{
                return inspections;
            })
        )
    };

    ngOnInit(): void {
        this._loadInspections();
    };

    _loadInspections():void{
        this.service.loadInspections().pipe(takeUntil(this.subscription$)).subscribe(
            response=>{
                if(response.success){
                    this.state.addInspections(
                        response.data
                    )
                }
            }
        )
    }

    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.subscription$.unsubscribe();
    }


}
