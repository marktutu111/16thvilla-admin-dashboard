import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { InspectionsComponent } from './inspections.component';
import { NbCardModule } from '@nebular/theme';
import { InspectionService } from '../../services/insepection.service';


const route:Routes=[
    {
        path:'',
        component:InspectionsComponent
    }
]

@NgModule({
    declarations: [
        InspectionsComponent
    ],
    imports: [ 
        CommonModule,
        NbCardModule,
        RouterModule.forChild(route)
    ],
    exports: [],
    providers: [],
})
export class InspectionsModule {}