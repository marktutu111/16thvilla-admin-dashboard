import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ApartmentInterface } from '../../../models/apartment.model';
import { PaymentType, RentType } from '../../../models/common.model';
import { RentService } from '../../../services/rent.service';
import { RoomService } from '../../../services/rooms.service';
import { TenantService } from '../../../services/tenants.service';
import { GenerateMonths } from '../../../utils/duration';
import { RoomState } from '../../states/rooms.state';
import { TenantState } from '../../states/tenant.state';

@Component({
    selector: 'app-add-rent',
    templateUrl: './add-rent.component.html',
    styleUrls: ['./add-rent.component.scss']
})
export class AddRentComponent implements OnInit,OnDestroy {

    rentType:RentType[]=[RentType.NEW,RentType.RENEWAL];
    paymentType:PaymentType[]=[PaymentType.CASH,PaymentType.MOMO,PaymentType['BANK DEPOSIT']];
    duration:any[]=GenerateMonths(1,36);
    formGroup:FormGroup;
    loading:boolean=false;
    private subscription$=new Subject();

    constructor(public tenants:TenantState,public properties:RoomState,
        private service:RentService,private rooms:RoomService,private ts:TenantService) {
        this.formGroup=new FormGroup(
            {
                "room":new FormControl('',Validators.required),
                "tenant":new FormControl('',Validators.required),
                "duration":new FormControl('',Validators.required),
                "costPerMonth":new FormControl(0,Validators.required),
                "paymentType":new FormControl('',Validators.required),
                "amount":new FormControl(0,Validators.required),
                'rentType':new FormControl('',Validators.required)
            }
        )
    };


    ngOnInit(): void {
        this.rooms.fetchRooms();
        this.ts.fetchTenants();
    };


    ngOnDestroy(): void {
        this.subscription$.next();
        this.subscription$.complete();
    };


    onProperty(d:string):void{
        const _d:ApartmentInterface=JSON.parse(d);
        this.formGroup.patchValue(
            {
                room:_d._id,
                costPerMonth:_d.price || 0
            }
        )
    }

    onDuration(duration:string){
        const count=parseInt(duration);
        this.formGroup.patchValue(
            {
                duration:duration,
                amount:count*this.formGroup.get('costPerMonth').value || 0
            }
        )
    }

    save(){
        const form=this.formGroup;
        if(!form.valid)return;
        this.loading=true;
        this.service.addRent(form.value)
                .pipe(takeUntil(this.subscription$))
                .subscribe(response=>{
                    this.loading=false;
                    this.formGroup.reset();
                    alert(response.message);
                })
    }

    


}
