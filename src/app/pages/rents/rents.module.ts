import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RentsComponent } from './rents.component';
import { NbCardModule, NbInputModule, NbSelectModule } from '@nebular/theme';
import { AllRentsComponent } from './allrents/allrents.component';
import { AddRentComponent } from './add-rent/add-rent.component';
import { RentService } from '../../services/rent.service';
import { ReactiveFormsModule } from '@angular/forms';
import { ApproveRentComponent } from './approve-rent/approve-rent.component';

const routes:Routes=[
    {
        path:'',
        component:RentsComponent,
        children:[
            {
                path:'',
                redirectTo:'all'
            },
            {
                path:'all',
                component:AllRentsComponent
            },
            {
                path:'addrent',
                component:AddRentComponent
            },
            {
                path:'approve',
                component:ApproveRentComponent
            }
        ]
    }
]

@NgModule({
    declarations: [
        RentsComponent,
        AllRentsComponent,
        AddRentComponent,
        ApproveRentComponent
    ],
    imports: [ 
        CommonModule,
        NbCardModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [],
})
export class RentsModule {}