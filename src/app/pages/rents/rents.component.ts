import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-rents',
    template: '<router-outlet></router-outlet>',
})
export class RentsComponent implements OnInit {
    constructor() {}

    ngOnInit(): void {}
    
}
