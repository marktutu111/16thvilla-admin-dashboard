import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription,combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { StatusType } from '../../../models/common.model';
import { RentInterface } from '../../../models/rents.model';
import { RentService } from '../../../services/rent.service';
import { StatusTypes } from '../../../utils/status';
import { FilterState } from '../../states/filter.state';
import { RentState } from '../../states/rents.state';

@Component({
    selector: 'app-allrents',
    templateUrl: './allrents.component.html',
    styleUrls: ['./allrents.component.scss']
})
export class AllRentsComponent implements OnInit,OnDestroy {

    filter=StatusTypes;
    tranStatus=StatusType;
    rents$:Observable<RentInterface[]>;
    private subscription$:Subscription;


    constructor(private rentState:RentState,private service:RentService,private filter$:FilterState) {
        this.rents$=combineLatest([this.rentState.rents$,this.rentState.filter$,this.filter$.searchTerm$]).pipe(
            map(([rents,filter,search]:[RentInterface[],StatusType,string])=>{
                let _filter=filter===StatusType.ALL?rents:rents.filter(r=>r.status===StatusType[filter]);
                return _filter.filter(r=>{
                    let name=`${r.tenant?.firstname} ${r.tenant?.lastname}`.toLowerCase();
                    return name.indexOf(search.toLowerCase())>-1;
                })
            })
        )
    }

    ngOnInit(): void {
        this.fetchRents();
    }

    fetchRents():void{
        this.subscription$=this.service.fetchRents().subscribe(
            response=>{
                if(response.success){
                    this.rentState.addRent(
                        response.data
                    )
                }
            }
        )
    }

    onFilter(e:Event|any){
        e.preventDefault();
        const el:StatusType=e.target.value;
        this.rentState.setFilter(
            el
        )
    }


    clearSearch(){
        this.rentState.onSearch(
            ''
        )
    }

    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.subscription$.unsubscribe();
    }

}
