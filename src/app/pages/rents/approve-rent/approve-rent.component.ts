import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RentInterface } from '../../../models/rents.model';
import { RentService } from '../../../services/rent.service';
import { RentState } from '../../states/rents.state';
import *as moment from "moment";
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-approve-rent',
    templateUrl: './approve-rent.component.html'
})
export class ApproveRentComponent implements OnInit {

    rentDetails:RentInterface;
    formGroup:FormGroup;
    loading:boolean=false;
    private subscription$=new Subject();

    constructor(public rents:RentState,private service:RentService) {
        this.formGroup=new FormGroup(
            {
                rentId:new FormControl('',Validators.required),
                tenantId:new FormControl('',Validators.required),
                startDate:new FormControl('',Validators.required),
                endDate:new FormControl('',Validators.required),
                status:new FormControl('',Validators.required),
                amount:new FormControl('',Validators.required),
                paymentType:new FormControl('',Validators.required)
            }
        )
    };

    ngOnInit(): void {
        this.service.fetchPending();
    }

    onRent(data:string){
        try {
            this.rentDetails=JSON.parse(data);
            this.formGroup.patchValue(
                {
                    rentId:this.rentDetails._id,
                    tenantId:this.rentDetails.tenant._id,
                    status:this.rentDetails.status,
                    amount:this.rentDetails.amount,
                    paymentType:this.rentDetails.paymentType,
                    startDate:moment(this.rentDetails.startDate).format('YYYY-MM-DD'),
                    endDate:moment(this.rentDetails.endDate).format('YYYY-MM-DD')
                }
            )
        } catch (err) {}
    }


    approve():void{
        if(!this.formGroup.valid){
            return;
        }
        this.loading=true;
        this.service.approveRent(this.formGroup.value)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                this.loading=false;
                alert(response.message);
                this.formGroup.reset();
            })
    }

    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.subscription$.next();
        this.subscription$.complete();
    }


}
