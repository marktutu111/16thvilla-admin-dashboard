import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { NetworkType, TransactionStatus } from '../../models/common.model';
import { DashboardInterface } from '../../models/dashboard.model';
import { ExpenseService } from '../../services/expense.service';
import { InvoiceService } from '../../services/invoice.service';
import { PaymentService } from '../../services/payments.service';
import { FilterState } from '../states/filter.state';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    colors:string[]=['bg-secondary','bg-warning','bg-info']
    tranStatus=TransactionStatus;
    networkType=NetworkType;
    constructor(
        public filter:FilterState,
        private payments:PaymentService,
        private expense:ExpenseService,
        private invoice:InvoiceService
    ) {};


    ngOnInit(): void {
        this.payments.fetchSummary();
        this.expense.loadSummary();
        this.invoice.loadSummary();
    }


}
