import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import *as moment from "moment";
import { ExpenseService } from '../../services/expense.service';
import { ExpenseState } from '../states/expenses.state';
import { map } from 'leaflet';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-expenses',
    templateUrl: './expenses.component.html',
    styleUrls: ['./expenses.component.scss']
})
export class ExpensesComponent implements OnInit {

    loading:Boolean=false;
    p:number=1;
    s:number=1;
    selected:any[]=[];

    formGroup:FormGroup;
    private subscription$=new Subject();


    constructor(private fb:FormBuilder,private _EService:ExpenseService,public state:ExpenseState) {
        this.formGroup=this.fb.group(
            {
                category:new FormGroup(
                    {
                        name:new FormControl(null,Validators.required),
                        description:new FormControl(null,Validators.required)
                    }
                ),
                expense:new FormGroup(
                    {
                        category:new FormControl(null,Validators.required),
                        amount:new FormControl(null,Validators.required),
                        tax:new FormControl(0),
                        reference:new FormControl(null),
                        notes:new FormControl(null,Validators.required),
                        expenseDate:new FormControl(new Date((new Date()).valueOf()).toLocaleDateString('en-CA'),Validators.required)
                    }
                ),
                filter:new FormGroup(
                    {
                        start:new FormControl(null),
                        end:new FormControl(null),
                        account:new FormControl(null)
                    }
                )
            }
        )
    }


    ngOnInit(): void {
        this._EService.getExpenses();
        this._EService.getExpenseCategory();
    };


    saveExpense(){
        const data=this.formGroup.get('expense');
        if(!data.valid){
            return;
        }
        this.loading=true;
        this._EService.addExpense(data.value).pipe(takeUntil(this.subscription$))
            .subscribe(response=>{
                this.loading=false;
                alert(response.message);
                if(response.success){
                    data.reset();
                    this._EService.getExpenses();
                    data.patchValue(
                        {
                            expenseDate:new Date((new Date()).valueOf()).toLocaleDateString('en-CA'),
                            tax:0
                        }
                    )
                }
            })
    }


    saveCategory(){
        const data=this.formGroup.get('category');
        if(!data.valid){
            return;
        }
        this.loading=true;
        this._EService.addExpenseCategory(data.value).pipe(takeUntil(this.subscription$))
            .subscribe(response=>{
                this.loading=false;
                alert(response.message);
                if(response.success){
                    data.reset();
                    this._EService.getExpenseCategory();
                }
            })
    }


    onDuration(event:any){
        try {
            const _duration:moment.Duration=event.value;
            if(typeof _duration!=='string')return;
            this.formGroup.get('filter').patchValue(
                {
                    start:moment(new Date()).startOf(_duration).toDate(),
                    end:moment(new Date()).endOf(_duration).toDate()
                }
            )
        } catch (err) {}
    }


    filter(){
        const data=this.formGroup.get('filter');
        this._EService.filterExpenses(
            data.value
        )
    }


    deleteExpense():void{
        if(!this.selected[0])return;
        this.loading=true;
        this._EService.deleteExpense(this.selected).pipe(takeUntil(this.subscription$))
            .subscribe(response=>{
                this.loading=false;
                alert(response.message);
                if(response.success){
                    this._EService.getExpenses();
                }

            })
    }


    deleteCategory():void{
        if(!this.selected[0])return;
        this.loading=true;
        this._EService.deleteExpenseCategory(this.selected).pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                this.loading=true;
                alert(response.message);
                if(response.success){
                    this._EService.getExpenseCategory();
                }
            })
    }


    onCheck(sel:any,id:string){
        const checked=sel.checked;
        switch (checked) {
            case false:
                this.selected=this.selected.filter(sel=>sel!==id);
                break;
            default:
                this.selected.push(id);
                break;
        }
    }


    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.subscription$.next();
        this.subscription$.complete();
    }



}
