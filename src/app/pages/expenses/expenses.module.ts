import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ExpensesComponent } from './expenses.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NbCardModule } from '@nebular/theme';
import { ExpenseService } from '../../services/expense.service';
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module


const routes:Routes=[
    {
        path:'',
        component:ExpensesComponent
    }
]

@NgModule({
    declarations: [
        ExpensesComponent
    ],
    imports: [ 
        CommonModule,
        NbCardModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [
        ExpenseService
    ],
})
export class ExpensesModule {}