import { NbMenuItem } from '@nebular/theme';


export const MENU_ITEMS: NbMenuItem[]=[
  {
    title: 'Menu',
    group: true,
  },
  {
    title: 'Dashboard',
    icon: 'layout-outline',
    link:'/home/dashboard'
  },
  {
    title: 'Apartments',
    icon: 'home',
    children: [
      {
        title: 'Available Rooms',
        link: '/home/apartments/available',
      },
      {
        title: 'Add Room',
        link: '/home/apartments/add',
      }
    ],
  },
  {
    title: 'Rents',
    icon: 'home-outline',
    children:[
      {
        title: 'All Rents',
        link: '/home/rents/all'
      },
      {
        title: 'Add Rents',
        link: '/home/rents/addrent'
      },
      {
        title: 'Approve Rents',
        link: '/home/rents/approve'
      }
    ]
  },
  {
    title: 'Tenants',
    icon: 'people',
    link:'/home/tenants'
  },
  {
    title: 'Payments',
    icon: 'credit-card-outline',
    link:'/home/payments'
  },
  {
    title: 'Inspections',
    icon: 'layout-outline',
    link:'/home/inspections'
  },
  {
    title: 'Expenses',
    icon: 'bar-chart-2-outline',
    link:'/home/expenses'
  },
  {
    title: 'Invoices',
    icon: 'calendar-outline',
    link:'/home/invoice'
  },
  {
    title: 'Settings',
    icon: 'settings-2-outline',
    link:'/home/settings'
  }
];
