import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { RoomState } from '../pages/states/rooms.state';
import { BASEURL } from '../utils/api';

@Injectable({
    providedIn: 'root'
})
export class RoomService {

    private subscription$=new Subject();

    constructor(private http:HttpClient,private state:RoomState){};

    fetchRooms():void{
        this.http.get(`${BASEURL}/rooms/all/get`)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                if(response.success){
                    this.state.addRooms(
                        response.data
                    )
                }
            })
    }

    uploadImage(data:any):Observable<any>{
        return this.http.post(`${BASEURL}/rooms/upload/image`,data);
    }

    addRoom(data:any):Observable<any>{
        return this.http.post(
            `${BASEURL}/rooms/new`,data
        )
    }

    upateRoom(data:any):Observable<any>{
        return this.http.put(
            `${BASEURL}/rooms/update`,data
        )
    }

    deleteRoom(id:any):Observable<any>{
        return this.http.delete(
            `${BASEURL}/rooms/delete/${id}`
        )
    }

    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.subscription$.next();
        this.subscription$.complete();
    }

}