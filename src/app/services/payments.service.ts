import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FilterState } from '../pages/states/filter.state';
import { BASEURL } from '../utils/api';

@Injectable({
    providedIn: 'root'
})
export class PaymentService {

    private subscription$=new Subject();
    constructor(private http:HttpClient,private filterState:FilterState){};

    fetchPayments():Observable<any>{
        return this.http.get(
            `${BASEURL}/payments/get`
        )
    }

    deleteTransaction(id:string):Observable<any>{
        return this.http.delete(`${BASEURL}/payments/delete/${id}`)
    }

    fetchSummary():void{
        this.http.get(`${BASEURL}/payments/aggregate`).pipe(takeUntil(this.subscription$))
        .subscribe((response:any)=>{
            if(response.success){
                this.filterState.updateDashboard(
                    response.data
                )
            }
        })
    }

    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.subscription$.next();
        this.subscription$.complete();
    }


}