import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SettingsInterface } from '../models/settings.model';
import { SettingsState } from '../pages/states/settings.state';
import { BASEURL } from '../utils/api';

@Injectable({
    providedIn: 'root'
})
export class SettingsService {

    private subscription$=new Subject();

    constructor(private http:HttpClient,private state:SettingsState){}

    sendSms(data:{message:string}):Observable<any>{
        return this.http.post(`${BASEURL}/settings/sms/push`,data)
    }

    loadSms():void{
        this.http.get(`${BASEURL}/settings/sms/get`)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                if(response.success && response.data){
                    this.state.addMessages(
                        response.data
                    )
                }
            })

    }

    loadUtilities():void{
        this.http.get(`${BASEURL}/utilities/get`)
                .pipe(takeUntil(this.subscription$))
                .subscribe((response:any)=>{
                    if(response.success && response.data){
                        this.state.addUtilities(
                            response.data
                        )
                    }
                })
    }


    loadSettings():void{
        this.http.get(`${BASEURL}/settings/get`)
                .pipe(takeUntil(this.subscription$))
                .subscribe((response:any)=>{
                    if(response.success && response.data){
                        this.state.addSettings(
                            response.data
                        )
                    }
                })
    }

    addUtility(data:any):Observable<any>{
        return this.http.post(`${BASEURL}/utilities/new`,data);
    }

    deleteUtility(id:string):Observable<any>{
        return this.http.delete(`${BASEURL}/utilities/delete/${id}`);
    }
    
    addSettings(data:SettingsInterface|any):Observable<any>{
        console.log(data);
        return this.http.put(`${BASEURL}/settings/add`,data);
    }

    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.subscription$.next();
        this.subscription$.complete();
    }

}