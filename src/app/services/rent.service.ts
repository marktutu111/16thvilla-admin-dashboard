import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { RentState } from '../pages/states/rents.state';
import { BASEURL } from '../utils/api';

@Injectable({
    providedIn: 'root'
})
export class RentService {

    private subscription$=new Subject();
    constructor(private http:HttpClient,private rents:RentState){};

    fetchRents():Observable<any>{
        return this.http.get(
            `${BASEURL}/rents/get`
        )
    }

    addRent(data:any):Observable<any>{
        return this.http.post(
            `${BASEURL}/rents/admin/new`,data
        )
    }

    approveRent(data:any):Observable<any>{
        return this.http.post(`${BASEURL}/rents/approve`,data)
    }

    fetchPending():void{
        this.http.get(`${BASEURL}/rents/pending/get`)
            .pipe(map((response:any)=>response.success?response.data:[]),takeUntil(this.subscription$))
            .subscribe(d=>{
                this.rents.setPending(
                    d
                )
            })
    }

    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.subscription$.next();
        this.subscription$.complete();
    }


}