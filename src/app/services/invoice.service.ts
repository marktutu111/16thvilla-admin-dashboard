import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Invoice, InvoiceModel, InvoicePayload } from '../models/invoice.model';
import { FilterState } from '../pages/states/filter.state';
import { InvoiceState } from '../pages/states/invoice.state';
import { BASEURL } from '../utils/api';

@Injectable({
    providedIn: 'root'
})
export class InvoiceService {

    private subscription$=new Subject();

    constructor(private http:HttpClient,private state:InvoiceState,private filter:FilterState){}

    getInvoices():void{
        this.http.get(`${BASEURL}/invoice/get`)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                if(response.success && response.data){
                    const {items,total,count}=response.data;
                    const _d=items.map((i:Invoice)=>new InvoiceModel(i));
                    this.state.addSummary({count,total})
                    this.state.addInvoices(
                        _d
                    )
                }
            })
                    
    }

    loadSummary():void{
        this.http.get(`${BASEURL}/invoice/summary/get`)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                if(response.success){
                    this.filter.updateInvoice(
                        response.data
                    )
                }
            })
    }

    addInvoice(data:InvoicePayload):Observable<any>{
        return this.http.post(`${BASEURL}/invoice/add`,data)
    }

    sendMoney(data:InvoicePayload):Observable<any>{
        return this.http.post(`${BASEURL}/invoice/sendmoney`,data)
    }


    payInvoice(id:string):Observable<any>{
        return this.http.post(`${BASEURL}/invoice/pay`,{ id:id })
    }
    
    
    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.subscription$.next();
        this.subscription$.complete();
    }

}