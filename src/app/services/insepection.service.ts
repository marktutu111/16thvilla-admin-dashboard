import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BASEURL } from '../utils/api';

@Injectable({
    providedIn: 'root'
})
export class InspectionService {
    
    constructor(private http:HttpClient){};

    loadInspections():Observable<any>{
        return this.http.get(
            `${BASEURL}/inspection/get`
        )
    }

}