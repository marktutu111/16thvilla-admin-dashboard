import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ExpenseModel, ExpensePayload } from '../models/expenses.model';
import { ExpenseState } from '../pages/states/expenses.state';
import { FilterState } from '../pages/states/filter.state';
import { BASEURL } from '../utils/api';

@Injectable({
    providedIn: 'root'
})
export class ExpenseService {

    subscription$=new Subject();

    constructor(private http:HttpClient,private state:ExpenseState,private filter:FilterState){}

    addExpense(data:ExpensePayload):Observable<any>{
        return this.http.post(`${BASEURL}/expenses/add`,data)
    }

    addExpenseCategory(payload:ExpensePayload):Observable<any>{
        return this.http.post(`${BASEURL}/expenses/category`,payload)
    }

    loadSummary():void{
        this.http.get(`${BASEURL}/expenses/summary/get`)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                if(response.success){
                    this.filter.updateExpense(
                        response.data
                    )
                }
            })
    }

    getExpenses():void{
        this.http.get(`${BASEURL}/expenses/get`)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                if(response.success && response.data){
                    const _d=response.data?.transactions.map((d)=>new ExpenseModel(d));
                    this.state.addExpenses(_d);
                    this.state.addSummary(
                        {
                            total:response.data?.total,
                            count:response.data?.count
                        }
                    )
                }
            })
    }

    filterExpenses(query:string):void{
        this.http.get(`${BASEURL}/expenses/filter?${query}`)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                if(response.success && response.data){
                    const _d=response.data.map(d=>new ExpenseModel(d));
                    this.state.addExpenses(_d);
                }
            })
    }

    getExpenseCategory():void{
        this.http.get(`${BASEURL}/expenses/category/get`).pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                if(response.success){
                    this.state.addCategory(
                        response.data
                    );
                }
            })
    }


    deleteExpenseCategory(data:string[]):Observable<any>{
        return this.http.post(`${BASEURL}/expenses/category/delete`,data)
    }


    deleteExpense(data:Array<string>):Observable<any>{
        return this.http.post(`${BASEURL}/expenses/delete`,data)
    }


    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.subscription$.next();
        this.subscription$.complete();

    }


}