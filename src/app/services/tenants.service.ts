import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TenantState } from '../pages/states/tenant.state';
import { BASEURL } from '../utils/api';

@Injectable({
    providedIn: 'root'
})
export class TenantService {

    private subscription$=new Subject();
    constructor(private http:HttpClient,private state:TenantState){};

    fetchTenants():void{
        this.http.get(`${BASEURL}/tenants/get`)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                if(response.success){
                    this.state.addTenents(
                        response.data
                    )
                }
            })
    }

    update(data:{id:string,data:any}):Observable<any>{
        return this.http.put(`${BASEURL}/tenants/update`,data)
    }

    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.subscription$.next();
        this.subscription$.complete();
    }

}