import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { TenantState } from '../pages/states/tenant.state';

@Injectable({
    providedIn: 'root'
})
export class TenantDetailsGuard implements CanActivate {
    constructor(private $tenant:TenantState,private router:Router){}
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        const selected=this.$tenant.selected$.value;
        if(!selected || !selected._id){
            this.router.navigate(['/home/tenants']);
            return false;
        }
        return true;
    }
}
