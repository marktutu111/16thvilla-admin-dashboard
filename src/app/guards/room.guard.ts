import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { RoomState } from '../pages/states/rooms.state';

@Injectable({
    providedIn: 'root'
})
export class RoomGuard implements CanActivate {
    constructor(private roomState:RoomState,private router:Router){};
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        const selected=this.roomState.selected$.value;
        if(!selected || !selected._id){
            this.router.navigate(['home/apartments/available']);
            return false;
        }
        return true;
    }
}
