import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { InvoiceState } from '../pages/states/invoice.state';

@Injectable({
    providedIn: 'root'
})
export class InvoiceGuard implements CanActivate {

    constructor(private state:InvoiceState,private router:Router){};
    canActivate(
        router: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ): Observable<boolean> | Promise<boolean> | boolean {
        const invoice=this.state.selected$.value;
        if(!invoice || !invoice._id){
            this.router.navigate(['../home/invoice']);
            return false;
        }
        return true;
    }
}
