import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationComponent } from './auth.component';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from '@angular/forms';

const route:Routes=[
    {
        path:'',
        redirectTo:'login',
        pathMatch:'full'
    },
    {
        path:'',
        component:AuthenticationComponent,
        children:[
            {
                path:'login',
                component:LoginComponent
            }
        ]
    }
]

@NgModule({
    declarations: [
        AuthenticationComponent,
        LoginComponent
    ],
    imports: [ 
        CommonModule,
        ReactiveFormsModule,
        RouterModule.forChild(route)
    ],
    exports: [],
    providers: [],
})
export class AuthModule {}