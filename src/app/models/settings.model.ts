
export interface SmsInterface {
    message:string;
}

export interface SettingsInterface {
    receive_sms_alerts:Array<string>;
    dollarRate:number;
    smsBalance:number;
}