export interface UtilitiesInterface {
    [index:string]:string|number;
    _id:string;
    type:string;
    amount:number;
    createdAt:string;
    updatedAt:string;
}