import { ApartmentInterface } from "./apartment.model";
import { AccountType, NetworkType, PaymentType, TransactionPurpose, TransactionStatus } from "./common.model";
import { InspectionInterface } from "./inspections.model";
import { RentInterface } from "./rents.model";
import { TenantInterface } from "./tenant.model";
import { UtilitiesInterface } from "./utilities.model";

export interface PaymentInterface {
    property:string;
    utilityId:UtilitiesInterface;
    tenantId:TenantInterface;
    rentId:RentInterface;
    inspectionId:InspectionInterface;
    propertyId:ApartmentInterface;
    purpose:TransactionPurpose;
    description:string;
    accountType:AccountType;
    accountNumber:String,
    accountIssuer:NetworkType;
    amount:number;
    datePaid:Date,
    status:TransactionStatus;
    paymentType:PaymentType
    period:string;
    duration:string;
    durationStartDate:string;
    durationEndDate:string;
    transaction:any;
    createdAt:string;
    updatedAt:string;
}