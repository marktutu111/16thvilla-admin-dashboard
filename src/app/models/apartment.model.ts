import { DurationType, PaymentType, RentType, StatusType } from "./common.model";

export interface ApartmentInterface {
    [index:string]:string | number;
    _id:string;
    room:string;
    tenant:string;
    duration:number;
    paymentType:PaymentType;
    amount:number;
    durationType:DurationType;
    status:StatusType;
    startDate:string;
    endDate:string;
    costPerMonth:number;
    rentType:RentType;
    durationRemaining:number;
    balanceRemaing:number;
    revokedReason:string;
    dateMoved:string;
    accomodation:string;
    createdAt:string;
    updatedAt:string;
}

