export type Gender='Male' | 'Female';

export enum PathTypes {
    'DETAILS'='details',
    'EDIT'='edit',
    'ADD'='add'
}


export enum NetworkType {
    'VODAFONE'='vodafone',
    'MTN'='mtn',
    'AIRTELTIGO'='airteltigo'
}

export enum AccountType {
    'momo'='momo',
    'card'='card'
}

export enum IDTYPES {
    'VOTER'='voter',
    'PASSPORT'='passport',
    'DL'='driverlicence',
    'GHANACARD'='ghanacard'
}


export enum HavePets {
    'YES'='YES',
    'NO'='NO'
}

export enum PaymentType {
    'CASH'='CASH',
    'MOMO'='MOMO',
    'BANK DEPOSIT'='BANK DEPOSIT'
}

export enum DurationType {
    'months'='months',
    'years'='years'
}


export enum TenantStatus {
    ALL='ALL',
    true='ACTIVE',
    false='PENDING'
}

export enum TransactionPurpose {
    'CASHOUT'='CASHOUT',
    'RENEWAL'='RENEWAL',
    'RENT'='RENT',
    'INSPECTION'='INSPECTION',
    'UTILITY'='UTILITY',
    'SD'='SD'
}

export enum TransactionStatus {
    'ALL'='ALL',
    'PAID'='PAID',
    'PENDING'='PENDING',
    'FAILED'='FAILED',
    'REVERSED'='REVERSED'
}

export enum StatusType {
    'ALL'='ALL',
    'DUE'='DUE',
    'PENDING APPROVAL'='PENDING APPROVAL',
    'EXPIRED'='EXPIRED',
    'ACTIVE'='ACTIVE',
    'REVOKED'='REVOKED',
    'PENDING PAYMENT'='PENDING PAYMENT'
}

export enum RentType {
    'NEW'='NEW',
    'RENEWAL'='RENEWAL'
}


export enum AccomodationType {
    'FOR SALE',
    'SHORT STAY',
    'LONG STAY'
}