import { ApartmentInterface } from "./apartment.model";
import { TransactionStatus } from "./common.model";


export interface InspectionInterface {
    [index:string]:any;
    room:ApartmentInterface;
    name:string;
    phone:string;
    date:string;
    time:string;
    status:TransactionStatus;
    amount:number;
    createdAt:string;
    updatedAt:string;
}