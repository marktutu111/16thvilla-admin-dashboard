
export interface ExpenseQueryType {
    [key:string]:string;
}


export interface ExpensePayload {
    category:string;
    amount:Partial<string | number>;
    tax:Partial<string | number>;
    reference:string;
    notes:string;
    expenseDate:string;
}

export interface ExpensesQuery {
    start:string;
    end:string;
    account:string;
}

export interface ExpenseResponse {
    total:number;
    count:number;
    transactions:Array<ExpenseModel>;
}


export interface ExpenseCategoryPayload {
    name:string;
    description:string;
}


export interface ExpenseCategory {
    _id:string;
    name:string;
    description:string;
    createdAt:string;
    updatedAt:string;
}


export interface Expense {
    _id:string;
    category:ExpenseCategory;
    amount:Partial<string | number>;
    tax:Partial<string | number>;
    reference:string;
    notes:string;
    createdAt:string;
    updatedAt:string;
    expenseDate:string;
}


export class ExpenseModel implements Expense {

    _id:string;
    category:ExpenseCategory;
    amount:Partial<string|number>;
    tax:Partial<string|number>;
    notes:string;
    reference:string;
    createdAt:string;
    updatedAt:string;
    expenseDate:string;

    constructor(expense:Expense){
        this._id=expense._id;
        this.category=expense.category;
        this.amount=expense.amount;
        this.notes=expense.notes;
        this.reference=expense.reference;
        this.tax=expense.tax;
        this.createdAt=expense.createdAt;
        this.updatedAt=expense.updatedAt;
        this.expenseDate=expense.expenseDate;
    }

}