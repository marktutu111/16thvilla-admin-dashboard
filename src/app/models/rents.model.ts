import { ApartmentInterface } from "./apartment.model";
import { AccomodationType, DurationType, PaymentType, RentType, StatusType } from "./common.model";
import { TenantInterface } from "./tenant.model";

export interface RentInterface {
    [index:string]:any;
    _id:string;
    room:ApartmentInterface;
    tenant:TenantInterface;
    duration:number;
    paymentType:PaymentType;
    amount:number;
    durationType:DurationType;
    status:StatusType;
    startDate:string;
    endDate:string;
    costPerMonth:number;
    rentType:RentType;
    durationRemaining:Number,
    balanceRemaing:Number,
    revokedReason:String,
    dateMoved:Date,
    accomodation:AccomodationType;
    createdAt:string;
    updatedAt:string;
}