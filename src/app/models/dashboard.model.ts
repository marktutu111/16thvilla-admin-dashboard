import { PaymentInterface } from "./payment.model";

export interface Aggregate {
    "_id":string;
    "total":number;
    "count":number;
    items?:PaymentInterface[];
}

export interface DashboardInterface {
    transactions:Aggregate[];
    today:Aggregate[];
    week:Aggregate[];
    year:Aggregate[];
    paymentType:Aggregate[];
    all:Aggregate[];
}