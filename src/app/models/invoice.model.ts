

export interface InvoicePayload {
    customerName:string;
    customerPhone:string;
    customerAddress:string;
    total:string;
    vat:string;
    subtotal:string;
    status:string;
    sales:Array<string>;
    item:string;
    description:string;
}


export interface Invoice {
    _id:string;
    customerName:string;
    customerPhone:string;
    customerAddress:string;
    item:string;
    description:string;
    quantity:string;
    amountPaid:string;
    total:string;
    vat:string;
    subtotal:string;
    status:string;
    datePaid:string,
    createdAt:string;
    updatedAt:string;
}


export class InvoiceModel implements Invoice {
    
    _id='';
    customerName='';
    customerPhone='';
    customerAddress='';
    item='';
    description='';
    quantity='';
    total='';
    vat='';
    subtotal='';
    status='';
    createdAt='';
    datePaid='';
    updatedAt='';
    amountPaid='';


    constructor(invoice:Invoice){
        this._id=invoice._id;
        this.customerName=invoice.customerName;
        this.customerPhone=invoice.customerPhone;
        this.customerAddress=invoice.customerAddress;
        this.item=invoice.item;
        this.description=invoice.description;
        this.total=invoice.total;
        this.quantity=invoice.quantity;
        this.amountPaid=invoice.amountPaid;
        this.vat=invoice.vat;
        this.subtotal=invoice.subtotal;
        this.status=invoice.status;
        this.createdAt=invoice.createdAt;
        this.datePaid=invoice.datePaid;
    }



}