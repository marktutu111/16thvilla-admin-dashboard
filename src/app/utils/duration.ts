export const GenerateMonths=(start:number,end:number)=>{
    let i=start;
    let durations=[];
    while(i<=end){
        durations.push(
            { 
                label:`${i} months`, 
                value:i 
            }
        )
        i++;
    }
    return durations;
}