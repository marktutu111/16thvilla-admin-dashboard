import { StatusType } from "../models/common.model"

export const StatusTypes:Array<StatusType>=[
    StatusType.ALL,
    StatusType.ACTIVE,
    StatusType.DUE,
    StatusType.EXPIRED,
    StatusType['PENDING APPROVAL'],
    StatusType['PENDING PAYMENT'],
    StatusType.REVOKED
]